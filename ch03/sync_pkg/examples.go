package sync_pkg

import (
	"fmt"
	"math"
	"os"
	"sync"
	"text/tabwriter"
	"time"
)

func WaitGroupExample() {
	var wg sync.WaitGroup
	wg.Add(1) // calls Add to track a goroutine exec
	go func() {
		defer wg.Done() // call Done to release a goroutine exec
		fmt.Println("1st goroutine sleeping...")
		time.Sleep(1 * time.Second)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("2nd goroutine sleeping...")
		time.Sleep(2 * time.Second)
	}()

	wg.Wait() // calls Wait to wait until all goroutines are done
	fmt.Println("All goroutines complete")
}

func AnotherWaitGroupExample() {
	hello := func(wg *sync.WaitGroup, id uint8) {
		defer wg.Done()
		fmt.Printf("Hello from %v!\n", id)
	}

	const numGreeters = 5
	var wg sync.WaitGroup
	wg.Add(numGreeters)
	for i := uint8(0); i < numGreeters; i++ {
		go hello(&wg, i+1)
	}
	wg.Wait()
}

func MutexExample() {
	var count int
	var lock sync.Mutex

	increment := func() {
		lock.Lock()
		defer lock.Unlock()
		count++
		fmt.Printf("Incrementing: %d\n", count)
	}

	decrement := func() {
		lock.Lock()
		defer lock.Unlock()
		count--
		fmt.Printf("Decrementing: %d\n", count)
	}

	var arithmetic sync.WaitGroup

	for i := 0; i <= 5; i++ {
		arithmetic.Add(1)
		go func() {
			defer arithmetic.Done()
			increment()
		}()
	}

	for i := 0; i <= 5; i++ {
		arithmetic.Add(1)
		go func() {
			defer arithmetic.Done()
			decrement()
		}()
	}

	arithmetic.Wait()
	fmt.Println("Arithmetic complete")
}

func RWMutexExample() {
	producer := func(wg *sync.WaitGroup, l sync.Locker) {
		defer wg.Done()
		for i := 5; i > 0; i-- {
			l.Lock()
			l.Unlock()
			time.Sleep(1 * time.Second)
		}
	}

	observer := func(wg *sync.WaitGroup, l sync.Locker) {
		defer wg.Done()
		l.Lock()
		defer l.Unlock()
	}

	test := func(count int, mutex, rwMutex sync.Locker) time.Duration {
		var wg sync.WaitGroup
		wg.Add(count + 1)
		beginTestTime := time.Now()
		go producer(&wg, mutex)
		for i := count; i > 0; i-- {
			go observer(&wg, rwMutex)
		}
		wg.Wait()
		return time.Since(beginTestTime)
	}

	tw := tabwriter.NewWriter(os.Stdout, 0, 1, 2, ' ', 0)
	defer tw.Flush()

	var m sync.RWMutex
	fmt.Fprintf(tw, "Readers\tRWMutex\t\tMutex\n")
	for i := 0; i < 20; i++ {
		count := int(math.Pow(2, float64(i)))
		fmt.Fprintf(
			tw,
			"%d\t%v\t%v\n",
			count,
			test(count, &m, m.RLocker()),
			test(count, &m, &m),
		)
	}
}

func CondExample() {
	c := sync.NewCond(&sync.Mutex{})    // 1. using a mutex as the Locker
	queue := make([]interface{}, 0, 10) // 2. slice with length 0 and capacity of 10

	removeFromQueue := func(delay time.Duration) {
		time.Sleep(delay)
		c.L.Lock()        // 8. enters again the critical section for the condition
		queue = queue[1:] // 9. simulates the dequeueing
		fmt.Println("Removed from queue")
		c.L.Unlock() // 10. exit the condition's critical section
		c.Signal()   // 11.
	}

	for i := 0; i < 10; i++ {
		c.L.Lock()            // 3. enter the critical section for the condition by calling Lock
		for len(queue) == 2 { // 4. check the length of the queue
			c.Wait() // 5. call Wait, which will suspend the goroutine until a signal
		}
		fmt.Println("Adding to queue")
		queue = append(queue, struct{}{})
		go removeFromQueue(1 * time.Second) // 6. create a goroutine that will dequeue an element
		c.L.Unlock()                        // 7. exit the condition's critical section
	}
}

func CondBroadcastExample() {
	type Button struct { // 1. struct with a condition (Cond)
		Clicked *sync.Cond
	}
	button := Button{
		Clicked: sync.NewCond(&sync.Mutex{}),
	}

	subscribe := func(cond *sync.Cond, fn func()) { // 2.
		var goroutineRunning sync.WaitGroup
		goroutineRunning.Add(1)
		go func() {
			goroutineRunning.Done()
			cond.L.Lock()
			defer cond.L.Unlock()
			cond.Wait()
			fn()
		}()
		goroutineRunning.Wait()
	}

	var clickRegistered sync.WaitGroup // 3.
	clickRegistered.Add(3)
	subscribe(button.Clicked, func() { // 4.
		fmt.Println("Maximizing window")
		clickRegistered.Done()
	})
	subscribe(button.Clicked, func() { // 5.
		fmt.Println("Displaying annoying dialog box!")
		clickRegistered.Done()
	})
	subscribe(button.Clicked, func() { // 6.
		fmt.Println("Mouse clicked.")
		clickRegistered.Done()
	})

	button.Clicked.Broadcast() // 7.
	clickRegistered.Wait()
}
