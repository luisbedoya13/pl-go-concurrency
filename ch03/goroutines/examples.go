package goroutines

import (
	"fmt"
	"runtime"
	"sync"
	"testing"
)

func ForkJoinExample() {
	var wg sync.WaitGroup // primitive for waiting for goroutines to finish
	sayHello := func() {
		defer wg.Done()
		fmt.Println("Hello")
	}

	wg.Add(1)
	go sayHello() // fork point
	wg.Wait()     // join point
}

func Closure() {
	var wg sync.WaitGroup
	salutation := "Hello"
	wg.Add(1)
	go func() { // executed within the same address space it was created
		defer wg.Done()
		salutation = "Greetings"
	}()
	wg.Wait()
	fmt.Println(salutation) // Greetings
}

func BadClosureOperation() {
	var wg sync.WaitGroup
	for _, salutation := range []string{"hello", "greetings", "good day"} {
		wg.Add(1)
		go func() {
			defer wg.Done()
			// variable hold from gc by goroutines, prints the last value 3 times
			fmt.Println(salutation)
		}()
	}
	wg.Wait()
}

func GoodClosureOperation() {
	var wg sync.WaitGroup
	for _, salutation := range []string{"hello", "greetings", "good day"} {
		wg.Add(1)
		go func(salutation string) { // pass the reference to goroutine
			defer wg.Done()
			fmt.Println(salutation)
		}(salutation)
	}
	wg.Wait()
}

func GoroutinesWeight() {
	memConsumed := func() uint64 {
		runtime.GC()
		var s runtime.MemStats
		runtime.ReadMemStats(&s)
		return s.Sys
	}

	var c <-chan interface{}
	var wg sync.WaitGroup
	noop := func() {
		wg.Done()
		<-c
	}

	const numGoroutines = 1e4
	wg.Add(numGoroutines)
	before := memConsumed()
	for i := numGoroutines; i > 0; i-- {
		go noop()
	}

	wg.Wait()
	after := memConsumed()
	fmt.Printf("%.3fkb", float64(after-before)/numGoroutines/1000)
}

func BenchmarkContextSwitch(b *testing.B) {
	var wg sync.WaitGroup
	begin := make(chan struct{})
	c := make(chan struct{})

	var token struct{}
	sender := func() {
		defer wg.Done()
		<-begin
		for i := 0; i < b.N; i++ {
			c <- token
		}
	}

	receiver := func() {
		defer wg.Done()
		<-begin
		for i := 0; i < b.N; i++ {
			<-c
		}
	}

	wg.Add(2)
	go sender()
	go receiver()
	b.StartTimer()
	close(begin)
	wg.Wait()
}
